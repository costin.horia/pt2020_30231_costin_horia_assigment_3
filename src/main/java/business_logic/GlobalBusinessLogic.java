package business_logic;

import data_access.ClientRepository;
import data_access.Order_PriceRepository;
import data_access.OrdersRepository;
import data_access.ProductRepository;
import model.Client;
import model.Orders;
import model.Product;
import presentation.ClientReportGenerator;
import presentation.OrderReportGenerator;
import presentation.PDFgenerator;
import presentation.ProductReportGenerator;

public class GlobalBusinessLogic {

	private final ClientRepository userRepository;
	private final ProductRepository prodrepos;
	private final OrdersRepository orderrepos;
	private final Order_PriceRepository pricerep;
	private final PDFgenerator gen;
	private final ClientReportGenerator clientgen;
	private final ProductReportGenerator prodgen;
	private final OrderReportGenerator ordergen;
	
	public GlobalBusinessLogic( ClientRepository userRepository, ProductRepository prodrepos, OrdersRepository orderrepos, PDFgenerator gen, Order_PriceRepository pricerep, ClientReportGenerator clientgen, ProductReportGenerator prodgen, OrderReportGenerator ordergen) {
		super();
		this.ordergen = ordergen;
		this.prodgen = prodgen;
		this.clientgen = clientgen;
		this.pricerep = pricerep;
		this.userRepository = userRepository;
		this.prodrepos =  prodrepos;
		this.orderrepos = orderrepos;
		this.gen = gen;
	}

	public void get_command(String comm)
	{
		String[] comm_comps = comm.split(" ");
		for(int i=0; i<comm_comps.length; i++)
		{
			System.out.print(comm_comps[i]+"+");
		}
		System.out.println("");
		switch(comm_comps[0]) {
		case "Insert":
			if(comm_comps[1].equals("client:"))
					{	
					System.out.println("Command Insert Client");
					String created_name = comm_comps[2]+" "+comm_comps[3];
					created_name=created_name.replace(",", "");
					System.out.println("OAH: "+created_name);
					String created_address = comm_comps[4].replace("\n", "").replace("\r", "");
					System.out.println("OAH: "+created_address);
					
					//System.out.println(this.userRepository.findByName(created_name).getName());
					if(this.userRepository.verifByName(created_name)==false)
					{
						this.insert_command_client(created_name, created_address);					
					}else {
						String compare_name = null;
						compare_name=this.userRepository.findByName(created_name).getName();
						if(created_name.equals(compare_name)==false )
						{
							this.insert_command_client(created_name, created_address);
							System.out.println("DA  DA");
						}
					}
					//
					}
			if(comm_comps[1].equals("product:"))
			{
				System.out.println("Command Insert Product");
				
				String created_name = comm_comps[2];
				created_name=created_name.replace(",", "");
				System.out.println("OAH: "+created_name);
				
				String created_quantity = comm_comps[3];
				created_quantity = created_quantity.replace(",", "");
				int quantity = Integer.parseInt(created_quantity);
				System.out.println("OAH: "+quantity);
				
				String created_price = comm_comps[4].replace("\n", "").replace("\r", "");
				Float price=Float.parseFloat(created_price);
				System.out.println("OAH: "+price);
				
				System.out.println("!"+created_name+"!");
				this.insert_command_product(created_name, quantity, price);
			}
			
			break;
		case "Delete":
			if(comm_comps[1].equals("client:"))
			{
					System.out.println("Command Delete Client");
					String name1 = comm_comps[2];
					String name2 = comm_comps[3];
					name2 = name2.replace(",", "");
					name2 = name2.replace("\n", "").replace("\r", "");
					String deleted_name = name1+" "+name2;
					System.out.println("Clientul pe care il stergem:-"+deleted_name+"-!");
					
					this.delete_command_client(deleted_name);
			}
			if(comm_comps[1].equals("product:"))
			{
					System.out.println("Command Delete Product");
					String product = comm_comps[2].replace("\n", "").replace("\r", "");
					System.out.println("Pordusul pe care il stergem:-"+product+"-!");
					this.delete_command_product(product);
			}
			break;
		case "Report":
			String comps=comm_comps[1].replace("\n", "").replace("\r", "");
			if(comps.equals("client"))
			{
				System.out.println("Generate Report Client");
				this.clientgen.generate_report_client();
			}
			if(comps.equals("product"))
			{
				System.out.println("Generate Report Product");
				this.prodgen.generate_report_product();
			}
			if(comps.equals("order"))
			{
				System.out.println("Generate Report Order");
				this.ordergen.generate_report_orders();
			}
			break;
		case "Order:":
			System.out.println("Command Order");
			String client_name = comm_comps[1] + " "+comm_comps[2];
			client_name=client_name.replace(",","");
			System.out.println("order client:"+client_name);
			
			String product_name = comm_comps[3].replace(",", "");
			System.out.println("order product:"+product_name);
			
			String q=comm_comps[4].replace("\n", "").replace("\r", "");
			int q_order =Integer.parseInt(q);
			System.out.println("order q:"+q_order);
			
			this.order_command(client_name, product_name, q_order);
			break;
		}
		System.out.println("//");
		System.out.println("//");
	}
	
	public void insert_command_client(String name, String address) {
		Client c=new Client(name,address);
		this.userRepository.create(c);
	}
	
	public void delete_command_client(String name) {
		this.userRepository.delete(name);
	}
	
	public void insert_command_product(String name, int quantity, float price) {
		Product prod = new Product(name, quantity, price);
		if( this.prodrepos.verifByName(name)==true )
		{
			System.out.println("DA ESTE");
			prod.setQuantity(prod.getQuantity() + quantity);
			this.prodrepos.update(prod);
		}else this.prodrepos.create(prod);
	}
	
	public void delete_command_product(String name) {
		this.prodrepos.delete(name);
	}
	
	public void order_command(String name_client, String name_product, int quantity) {
		Orders order = new Orders(name_client, name_product, quantity); 

		System.out.println("!"+order.getId()+"!"+order.getName_client()+"!"+order.getName_product()+"!"+order.getQuantity()+"!");
		//orderrepos.create(order);
		
		
		Product prod = this.prodrepos.findByName(name_product);
		if(prod.getQuantity() > quantity )
		{
		prod.setQuantity(prod.getQuantity() - quantity );
		this.prodrepos.update(prod);
		orderrepos.create(order);
		
		Long id_order = order.getId();
		float total = prod.getPrice() * quantity;
		
		pricerep.create(id_order, total);
		
		this.gen.generator_order_pdf(order, total);
		
		}else {
			System.out.println("Stoc INSUFICIENT!");
			gen.under_stock_generator(name_product, prod.getQuantity());
		}
	}
	
}
