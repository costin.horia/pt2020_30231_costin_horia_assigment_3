package data_access;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/*Clasa care imi face conexiunea la serverul mysql
*si imi creeaza tabelele: client, product, order, order_price
*/

public class JDBConnectionWrapper {
	private static final String DB_URL = "jdbc:mysql://localhost:3306/";
    private static final String USER = "root";
    private static final String PASS = "";
    private static final int TIMEOUT = 5;

    private Connection connection;

    public JDBConnectionWrapper(String schemaName) {
        try {
            connection = DriverManager.getConnection(DB_URL + schemaName + "?useSSL=false", USER, PASS);
            createTables();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void createTables() throws SQLException {
        Statement statement = connection.createStatement();

        String sql = "CREATE TABLE IF NOT EXISTS client (" +
                "  id BIGINT(100) NOT NULL AUTO_INCREMENT," +
                "  name varchar(255) NOT NULL," +
                "  address varchar(255) NOT NULL," +
                "  PRIMARY KEY (id)," +
                "  UNIQUE KEY id_UNIQUE (id)" +
                ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;";
        statement.execute(sql);
        
        sql = "CREATE TABLE IF NOT EXISTS product (" +
                "  id BIGINT(100) NOT NULL AUTO_INCREMENT," +
                "  name_product varchar(255) NOT NULL," +
                "  quantity INT(100) NOT NULL," +
                "  price DOUBLE NOT NULL," +
                "  PRIMARY KEY (id)," +
                "  UNIQUE KEY id_UNIQUE (id)" +
                ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;";
        statement.execute(sql);
        
        sql = "CREATE TABLE IF NOT EXISTS orders (" + 
                "  id BIGINT(100) NOT NULL AUTO_INCREMENT," +
                "  name_client varchar(255) NOT NULL," +
                "  name_product varchar(255) NOT NULL," +
                "  quantity INT(100) NOT NULL," +
                "  PRIMARY KEY (id)," +
                "  UNIQUE KEY id_UNIQUE (id)" +
                ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;";
        statement.execute(sql);
        
        sql = "CREATE TABLE IF NOT EXISTS order_price (" + 
                "  id BIGINT(100) NOT NULL," +
                "  total DOUBLE NOT NULL," +
                "  PRIMARY KEY (id)," +
                "  UNIQUE KEY id_UNIQUE (id)" +
                ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;";
        statement.execute(sql);
    }

    public boolean testConnection() throws SQLException {
        return connection.isValid(TIMEOUT);
    }

    public Connection getConnection() {
        return connection;
    }
}
