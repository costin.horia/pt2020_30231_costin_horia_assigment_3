package data_access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/*Clasa care imi executa query - urile pentru tabelul order_price
*/
public class Order_PriceRepository {
	private final JDBConnectionWrapper jdbConnectionWrapper;

    public Order_PriceRepository(JDBConnectionWrapper jdbConnectionWrapper) {
        this.jdbConnectionWrapper = jdbConnectionWrapper;
    }
    
    public Float create(Long id_order, Float total) {
        Connection connection = jdbConnectionWrapper.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO order_price (id ,total) VALUES(?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setLong(1, id_order);
            preparedStatement.setFloat(2, total);

            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if(resultSet.next()) {
                return resultSet.getFloat(2);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public boolean delete_all() {
        Connection connection = jdbConnectionWrapper.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM order_price ");

            int updatedRows = preparedStatement.executeUpdate();

            return updatedRows > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
