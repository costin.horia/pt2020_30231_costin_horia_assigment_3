package data_access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Product;


public class ProductRepository {
	private final JDBConnectionWrapper jdbConnectionWrapper;

    public ProductRepository(JDBConnectionWrapper jdbConnectionWrapper) {
        this.jdbConnectionWrapper = jdbConnectionWrapper;
    }
    
    public Product create(Product prod) {
        Connection connection = jdbConnectionWrapper.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO product (name_product ,quantity ,price) VALUES(?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, prod.getName_product());
            preparedStatement.setInt(2, prod.getQuantity());
            preparedStatement.setFloat(3, prod.getPrice());

            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if(resultSet.next()) {
                prod.setId(resultSet.getLong(1));
                return prod;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Product update(Product p) {
        Connection connection = jdbConnectionWrapper.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE product SET name_product=?, quantity=?, price=? WHERE name_product=?",
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, p.getName_product());
            preparedStatement.setInt(2, p.getQuantity());
            preparedStatement.setDouble(3, p.getPrice());
            preparedStatement.setString(4, p.getName_product());

            int changedRows = preparedStatement.executeUpdate();

            if(changedRows > 0) {
                return p;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean delete(String name) {
        Connection connection = jdbConnectionWrapper.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM product WHERE name_product= ?");
            preparedStatement.setString(1, name);

            int updatedRows = preparedStatement.executeUpdate();

            return updatedRows > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public boolean delete_all() {
        Connection connection = jdbConnectionWrapper.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM product ");

            int updatedRows = preparedStatement.executeUpdate();

            return updatedRows > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

	public Product findByName(String name) {
		Connection connection = jdbConnectionWrapper.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM product WHERE name_product=?");
            preparedStatement.setString(1, name);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Product extractedAcc = new Product();

                extractedAcc.setId(resultSet.getLong(1));
                extractedAcc.setName_product(resultSet.getString(2));
                extractedAcc.setQuantity(resultSet.getInt(3));
                extractedAcc.setPrice(resultSet.getFloat(4));

                return extractedAcc;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
	}
	
	public ArrayList<Product> findAll() {
        Connection connection = jdbConnectionWrapper.getConnection();
        ArrayList<Product> products = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM product");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Product product = new Product();

                product.setId(resultSet.getLong(1));
                product.setName_product(resultSet.getString(2));
                product.setPrice(resultSet.getFloat(4));
                product.setQuantity(resultSet.getInt(3));
               
                products.add(product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return products;
    }
	
	public boolean verifByName(String name) {
		Connection connection = jdbConnectionWrapper.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM product WHERE name_product=?");
            preparedStatement.setString(1, name);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()==true) {
                /*Client extractedAcc = new Client();

                extractedAcc.setId(resultSet.getLong(1));
                extractedAcc.setName(resultSet.getString(2));
                extractedAcc.setAddress(resultSet.getString(3));

                return extractedAcc;*/
            	return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
	}
}
