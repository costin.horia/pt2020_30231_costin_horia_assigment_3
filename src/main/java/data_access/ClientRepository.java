package data_access;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;



import model.Client;

/*Clasa care imi executa query - urile pentru tabelul de clienti
*/

public class ClientRepository {
	private final JDBConnectionWrapper jdbConnectionWrapper;

    public ClientRepository(JDBConnectionWrapper jdbConnectionWrapper) {
        this.jdbConnectionWrapper = jdbConnectionWrapper;
    }
    
    public Client create(Client client) {
        Connection connection = jdbConnectionWrapper.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO client (id ,name ,address) VALUES(?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setLong(1, client.getId());
            preparedStatement.setString(2, client.getName());
            preparedStatement.setString(3, client.getAddress());

            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if(resultSet.next()) {
                client.setId(resultSet.getLong(1));
                return client;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public boolean delete(String name) {
        Connection connection = jdbConnectionWrapper.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM client WHERE name= ?");
            preparedStatement.setString(1, name);

            int updatedRows = preparedStatement.executeUpdate();

            return updatedRows > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public boolean delete_all() {
        Connection connection = jdbConnectionWrapper.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM client");

            int updatedRows = preparedStatement.executeUpdate();

            return updatedRows > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

	public Client findByName(String name) {
		Connection connection = jdbConnectionWrapper.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM client WHERE name=?");
            preparedStatement.setString(1, name);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                Client extractedAcc = new Client();

                extractedAcc.setId(resultSet.getLong(1));
                extractedAcc.setName(resultSet.getString(2));
                extractedAcc.setAddress(resultSet.getString(3));

                return extractedAcc;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
	}
	
	
	public ArrayList<Client> findAll() {
        Connection connection = jdbConnectionWrapper.getConnection();
        ArrayList<Client> articles = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM client");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Client client = new Client();

                client.setId(resultSet.getLong(1));
                client.setName(resultSet.getString(2));
                client.setAddress(resultSet.getString(3));
               
                articles.add(client);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return articles;
    }
	
	public boolean verifByName(String name) {
		Connection connection = jdbConnectionWrapper.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM client WHERE name=?");
            preparedStatement.setString(1, name);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()==true) {
                /*Client extractedAcc = new Client();

                extractedAcc.setId(resultSet.getLong(1));
                extractedAcc.setName(resultSet.getString(2));
                extractedAcc.setAddress(resultSet.getString(3));

                return extractedAcc;*/
            	return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
	}

}
