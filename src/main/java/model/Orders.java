package model;

public class Orders {
	private long id;
	private String name_client;
	private String name_product;
	private int quantity;
	
	
	
	public Orders() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Orders(String name_client, String name_product, int quantity) {
		super();
		this.name_client = name_client;
		this.name_product = name_product;
		this.quantity = quantity;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName_client() {
		return name_client;
	}
	public void setName_client(String name_client) {
		this.name_client = name_client;
	}
	public String getName_product() {
		return name_product;
	}
	public void setName_product(String name_product) {
		this.name_product = name_product;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
	
}
