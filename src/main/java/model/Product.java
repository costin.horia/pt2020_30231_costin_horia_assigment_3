package model;

public class Product {
	private Long id;
	private String name_product;
	private int quantity;
	private float price;
	
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Product(String name_product, int quantity, float price) {
		super();
		this.name_product = name_product;
		this.quantity = quantity;
		this.price = price;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName_product() {
		return name_product;
	}

	public void setName_product(String name_product) {
		this.name_product = name_product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	

}
