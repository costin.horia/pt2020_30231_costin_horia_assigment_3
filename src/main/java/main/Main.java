package main;

import business_logic.GlobalBusinessLogic;
import data_access.ClientRepository;
import data_access.JDBConnectionWrapper;
import data_access.Order_PriceRepository;
import data_access.OrdersRepository;
import data_access.ProductRepository;
import presentation.ClientReportGenerator;
import presentation.FileParser;
import presentation.OrderReportGenerator;
import presentation.PDFgenerator;
import presentation.ProductReportGenerator;


public class Main {
	private static JDBConnectionWrapper jdbConnectionWrapper;
	private static ClientRepository userRepository;
	private static GlobalBusinessLogic globalbl;
	private static ProductRepository productrepository;
	private static OrdersRepository ordersrepository;
	private static Order_PriceRepository order_pricerepository;
	private static PDFgenerator gen;
	private static ClientReportGenerator clientgen;
	private static ProductReportGenerator prodgen;
	private static OrderReportGenerator ordergen;

	public static void main(String[] args) {

		jdbConnectionWrapper = new JDBConnectionWrapper("assig3_tp");
		userRepository = new ClientRepository(jdbConnectionWrapper);
		productrepository = new ProductRepository(jdbConnectionWrapper);
		ordersrepository = new OrdersRepository(jdbConnectionWrapper);
		order_pricerepository = new Order_PriceRepository(jdbConnectionWrapper);
		
		gen = new PDFgenerator(userRepository,productrepository);
		clientgen = new ClientReportGenerator(userRepository);
		prodgen = new ProductReportGenerator(productrepository);
		ordergen = new OrderReportGenerator(ordersrepository);
		
		globalbl = new GlobalBusinessLogic(userRepository, productrepository, ordersrepository, gen, order_pricerepository, clientgen, prodgen, ordergen);
		
		
		order_pricerepository.delete_all();
		productrepository.delete_all();
		userRepository.delete_all();
		ordersrepository.delete_all();
		
		//"D:\\PT2020\\assig3\\Ex1.txt"
		FileParser p = new FileParser(args[0]);
		String[] s=p.getCommands();
		
		
		for(int i=0;i<s.length;i++) { //System.out.println("-> "+s[i]);
		globalbl.get_command(s[i]); }
		
	}

}
