package presentation;

import java.nio.file.Files;
import java.nio.file.Paths;

public class FileParser {
	
	private String[] commands;
	//private static String filename;

	public FileParser(String filename) {
		super();
		// TODO Auto-generated constructor stub
		//this.ReadFromFile();
		this.commands = ReadFromFile(filename);
		
	}

	public static String readFileAsString(String fileName)throws Exception 
	  { 
	    String data = ""; 
	    data = new String(Files.readAllBytes(Paths.get(fileName))); 
	    return data; 
	  } 
	
	public String[] ReadFromFile(String filename) {
		String data = "";
		
		try {
			data = readFileAsString(filename);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		    //System.out.println(data); 
		    String[] s=data.split("\n");
		  //System.out.println("->"+Arrays.toString(s));
		    
		/*
		 * for(int i=0;i<s.length;i++) { System.out.println("-> "+s[i]); }
		 */
		    return s;
		}

	public String[] getCommands() {
		return commands;
	}

	public void setCommands(String[] commands) {
		this.commands = commands;
	}
	
	
}
