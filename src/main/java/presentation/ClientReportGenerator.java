package presentation;

import java.io.FileOutputStream;
import java.util.ArrayList;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import data_access.ClientRepository;
import model.Client;

public class ClientReportGenerator {
	ClientRepository clientrepository;
	static int nr=0;
	
	public ClientReportGenerator(ClientRepository clientrepository) {
		super();
		this.clientrepository = clientrepository;
	}

	public void generate_report_client() {
		Document document = new Document();
		nr++;
	    try
	    {
	    	ArrayList<Client> clients = this.clientrepository.findAll();
	    	
	    	
	    	String title="Client Report "+nr+".pdf";
	        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(title));
	        document.open();
	 
	        PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	 
	        //Set Column widths
	        float[] columnWidths = {1f, 1f, 1f};
	        table.setWidths(columnWidths);
	        
	        Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
	        Phrase id_text = new Phrase("ID",boldFont);
	        PdfPCell cell1 = new PdfPCell(id_text);
	        cell1.setPaddingLeft(10);
	        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
	        cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
	        cell1.setBorderWidth(2);
	        
	        Phrase name_text = new Phrase("Name",boldFont);
	        PdfPCell cell2 = new PdfPCell(name_text);
	        cell2.setPaddingLeft(10);
	        cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
	        cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
	        cell2.setBorderWidth(2);

	        
	        Phrase address_text = new Phrase("Address",boldFont);
	        PdfPCell cell3 = new PdfPCell(address_text);
	        cell3.setPaddingLeft(10);
	        cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
	        cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
	        cell3.setBorderWidth(2);
	 
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);	        
	        
	        for (Client e:clients) {
	        	
	        	Long id = e.getId();
	        	PdfPCell cell_id = new PdfPCell(new Paragraph(id.toString()  ));
		        cell_id.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell_id.setVerticalAlignment(Element.ALIGN_MIDDLE);
	        	
	        	PdfPCell cell_name = new PdfPCell(new Paragraph(e.getName()));
		        cell_name.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell_name.setVerticalAlignment(Element.ALIGN_MIDDLE);
	        	
	        	PdfPCell cell_address = new PdfPCell(new Paragraph(e.getAddress()));
		        cell_address.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell_address.setVerticalAlignment(Element.ALIGN_MIDDLE);
	        	
	        	table.addCell(cell_id);
		        table.addCell(cell_name);
		        table.addCell(cell_address);

	        }
	        
	        document.add(table);
	 
	        document.close();
	        writer.close();
	    } catch (Exception e)
	    {
	        e.printStackTrace();
	    }
	}
}
