package presentation;

import java.io.FileOutputStream;
import java.util.ArrayList;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import data_access.ProductRepository;
import model.Product;

public class ProductReportGenerator {
	ProductRepository productrepository;
	static int nr=0;
	
	public ProductReportGenerator(ProductRepository productrepository) {
		super();
		this.productrepository = productrepository;
	}

	public void generate_report_product() {
		Document document = new Document();
		nr++;
	    try
	    {
	    	ArrayList<Product> products = this.productrepository.findAll();
	    	
	    	String title="Product Report "+nr+".pdf";
	        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(title));
	        document.open();
	        
	        PdfPTable table = new PdfPTable(4); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        
	      //Set Column widths
	        float[] columnWidths = {1f, 1f, 1f, 1f};
	        table.setWidths(columnWidths);
	        
	        Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
	        Phrase id_text = new Phrase("ID",boldFont);
	        PdfPCell cell1 = new PdfPCell(id_text);
	        cell1.setPaddingLeft(10);
	        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
	        cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
	        cell1.setBorderWidth(2);
	        
	        Phrase name_text = new Phrase("Name product",boldFont);
	        PdfPCell cell2 = new PdfPCell(name_text);
	        cell2.setPaddingLeft(10);
	        cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
	        cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
	        cell2.setBorderWidth(2);

	        
	        Phrase quantity_text = new Phrase("Quantity",boldFont);
	        PdfPCell cell3 = new PdfPCell(quantity_text);
	        cell3.setPaddingLeft(10);
	        cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
	        cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
	        cell3.setBorderWidth(2);
	        
	        Phrase price_text = new Phrase("Price",boldFont);
	        PdfPCell cell4 = new PdfPCell(price_text);
	        cell4.setPaddingLeft(10);
	        cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
	        cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
	        cell4.setBorderWidth(2);
	 
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell4);
	        table.addCell(cell3);

	        
	        for(Product e:products) {
	        	System.out.println("Porduct: "+e.getName_product()+", "+e.getPrice()+", "+e.getQuantity());
	        	
	        	
	        	Long id = e.getId();
	        	PdfPCell cell_id = new PdfPCell(new Paragraph(id.toString()  ));
		        cell_id.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell_id.setVerticalAlignment(Element.ALIGN_MIDDLE);
		        
		        PdfPCell cell_name = new PdfPCell(new Paragraph(e.getName_product()  ));
		        cell_name.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell_name.setVerticalAlignment(Element.ALIGN_MIDDLE);
		        
		        float price = e.getPrice();
		        PdfPCell cell_price = new PdfPCell(new Paragraph( String.valueOf(price) ));
		        cell_price.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell_price.setVerticalAlignment(Element.ALIGN_MIDDLE);
		        
		        int q = e.getQuantity();
		        //System.out.println(q);
		        PdfPCell cell_quantity = new PdfPCell(new Paragraph( String.valueOf(q) ));
		        cell_quantity.setHorizontalAlignment(Element.ALIGN_CENTER);
		        cell_quantity.setVerticalAlignment(Element.ALIGN_MIDDLE);
		        
		        table.addCell(cell_id);
		        table.addCell(cell_name);
		        table.addCell(cell_price);
		        table.addCell(cell_quantity);

	        }
	        document.add(table);
	   	 
	        document.close();
	        writer.close();
	        
	    }catch (Exception e)
	    {
	        e.printStackTrace();
	    }
	    
		}
}
