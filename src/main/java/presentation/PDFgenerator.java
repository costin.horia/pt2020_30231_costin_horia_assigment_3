package presentation;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import data_access.ClientRepository;
import data_access.ProductRepository;
import model.Orders;

public class PDFgenerator {
	ClientRepository clientrepository;
	ProductRepository productrepository;
	static int nr=0;
	
	public PDFgenerator(ClientRepository clientrepository,ProductRepository productrepository) {
		super();
		this.clientrepository = clientrepository;
		this.productrepository = productrepository;
	}

	public void generator_order_pdf(Orders order, float sum) {
		Document document = new Document();
		
		try
	      {
			Long nrbill = order.getId(); 
			String bill ="Bill"+nrbill.toString();
			String title = bill+".pdf";
			System.out.println("Titlu"+title);
	         PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(title));
	         document.open();
	         document.add(new Paragraph("Order bill number: "+order.getId()));
	         document.addTitle("Order bill number: "+order.getId());
	         document.add(new Paragraph("Client name: "+order.getName_client()));
	         document.add(new Paragraph("Product: "+order.getName_product()));
	         document.add(new Paragraph("Quantity: "+order.getQuantity()));
	         document.add(new Paragraph("Total: "+sum));
	         document.close();
	         writer.close();
	      } catch (DocumentException e)
	      {
	         e.printStackTrace();
	      } catch (FileNotFoundException e)
	      {
	         e.printStackTrace();
	      }
	}
	
	public void under_stock_generator(String product, int stock_available) {
		Document document = new Document();
		nr++;
		
		try
	      {
		String title = "Under-stock msg "+nr+".pdf";
		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(title));
        document.open();
        document.add(new Paragraph("We are sorry but we dont enough on stock for the product: "+product));
        document.add(new Paragraph("Stock available: "+Integer.toString(stock_available) ));
        document.close();
        writer.close();
     } catch (DocumentException e)
     {
        e.printStackTrace();
     } catch (FileNotFoundException e)
     {
        e.printStackTrace();
     }
	}
	
	
}
